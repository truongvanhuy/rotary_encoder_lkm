#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>

#define DEVICE_NAME "rotary_encoder"
#define CLASS_NAME "re"

#define CLK	25
#define DT	22
#define SW	14

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Truong Van Huy");
MODULE_DESCRIPTION("A rotary encoder driver");
MODULE_VERSION("0.1");

static int majorNumber;
static struct class* recharClass = NULL;
static struct device* recharDevice = NULL;
static char angleString[4] = {0};

int counter = 0;
int angle = 0;
int state;
int lastState;
int irqNumber;

static int      dev_open(struct inode *, struct file *);
static int      dev_release(struct inode *, struct file *);
static ssize_t  dev_read(struct file *, char *, size_t, loff_t *);

static struct file_operations fops =
{
        .open = dev_open,
        .read = dev_read,
        .release = dev_release,
};

static irq_handler_t re_irq_handler(unsigned int irq, void *dev_id, struct pt_regs *regs) {
	state = gpio_get_value(CLK);
	if (state != lastState) {
		if (gpio_get_value(DT) != state) {
			counter++;
			angle++;
		} else {
			counter--;
			angle--;
		}
		if (counter >= 30)
        	        counter = 0;
	}

	lastState = state;
        printk(KERN_INFO "angle = %d\n", (int)(angle*(-12)));
	return (irq_handler_t)IRQ_HANDLED;
}

static int __init re_init(void) {
	int result = 0;

	printk(KERN_INFO "RE: Initializing the rotary encoder LKM\n");

	majorNumber = register_chrdev(0, DEVICE_NAME, &fops);
	if (majorNumber < 0) {
		printk(KERN_ALERT "RE failed to register a major number\n");
		return majorNumber;
	}

	recharClass = class_create(THIS_MODULE, CLASS_NAME);
	if (IS_ERR(recharClass)) {
		unregister_chrdev(majorNumber, DEVICE_NAME);
		printk(KERN_ALERT "Failed to register device class\n");
		return PTR_ERR(recharClass);
	}

	recharDevice = device_create(recharClass, NULL, MKDEV(majorNumber, 0), NULL, DEVICE_NAME);
	if (IS_ERR(recharDevice)) {
		class_destroy(recharClass);
		unregister_chrdev(majorNumber, DEVICE_NAME);
		printk(KERN_ALERT "Failed to create the device\n");
		return PTR_ERR(recharDevice);
	}

	if (!gpio_is_valid(DT)) {
		printk(KERN_INFO "DT: invalid gpio\n");
		return -ENODEV;
	}

	if (!gpio_is_valid(SW)) {
                printk(KERN_INFO "SW: invalid gpio\n");
                return -ENODEV;
        }

	gpio_request(DT, "sysfs");
	gpio_direction_input(DT);
	gpio_export(DT, false);

	gpio_request(SW, "sysfs");
	gpio_direction_input(SW);
	gpio_export(SW,false);

	gpio_request(CLK, "sysfs");
	gpio_direction_input(CLK);
	gpio_set_debounce(CLK, 50);
	gpio_export(CLK, false);

	irqNumber = gpio_to_irq(CLK);
	result = request_irq(irqNumber,
			(irq_handler_t)re_irq_handler,
			IRQF_TRIGGER_RISING | IRQF_TRIGGER_FALLING,
			"re_gpio_clk_handler",
			NULL);
	printk(KERN_INFO "GPIO_TEST: The interrupt request result is: %d\n", result);
	return result;
}

static void __exit re_exit(void) {
	device_destroy(recharClass, MKDEV(majorNumber, 0));
	class_unregister(recharClass);
	unregister_chrdev(majorNumber, DEVICE_NAME);
	gpio_set_value(SW,0);
	gpio_unexport(SW);
	gpio_set_value(DT,0);
        gpio_unexport(DT);
	free_irq(irqNumber, NULL);
	gpio_unexport(CLK);
	gpio_free(CLK);
	gpio_free(SW);
	gpio_free(DT);
	printk(KERN_INFO "Rotary encoder: goodbyte!\n");
}

static int dev_open(struct inode *inodep, struct file *filep) {
	return 0;
}

static ssize_t dev_read(struct file *filep, char *buffer, size_t len, loff_t *offset) {
	int error_count = 0;
	snprintf(angleString, 4, "%d", angle*(-12));
	error_count = copy_to_user(buffer, angleString, strlen(angleString));
	if (error_count == 0) {
		return 0;
	} else {
		printk(KERN_INFO "Faild to send characters to the user\n");
		return -EFAULT;
	}
}

static int dev_release(struct inode *inodep, struct file *filep) {
	return 0;
}
module_init(re_init);
module_exit(re_exit);

